![](https://gaforgithub.azurewebsites.net/api?repo=CKAD-exercises/pod_design&empty)
# Horizontal Pod Autoscaler

### Autoscale the deployment, pods between 5 and 10, targetting CPU utilization at 80%

<details><summary>show</summary>
<p>

```bash
kubectl autoscale deploy nginx --min=5 --max=10 --cpu-percent=80
```

</p>
</details>

### Get the YAML for the HPA

<details><summary>show</summary>
<p>

```bash
kubectl get hpa nginx -o yaml 
```

</p>
</details>